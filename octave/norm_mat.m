function a = norm_mat(a)
 % Normalize a matrix.
 % Copyright Malte Ahl.
 s = sum(a);
 a /= diag(s); 
end
