function result = softmax(vec)
% Calculates the softmax value row-wise.
% Copyright Malte Ahl
  for i = [1:size(vec)(1)]
    for j = [1:size(vec)(2)]
      v = vec(i,j);
      if v > 0
        result(i,j) = exp(vec(i,j));
      else 
        result(i,j) = 0;
      end
    end
    sum_i = sum(result(i,:));
    if sum_i > 0
      result(i,:) = result(i, :) ./ sum_i;
    else 
      result(i,:) = zeros(1, size(vec)(2));
    end
  end
end
