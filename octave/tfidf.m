function score = tfidf(run='', tf, b, k, noi, ais, N, df)
% The tfidf function as used by bm25().
% Copyright Malte Ahl.
   tf_star = tf * (k + 1) / (k * (1 - b + b * noi / ais) + tf);

  %Compute the tf_idf value.
  %score = tf_star * log2(N / df);
  %score = tf_star * (1 - df / (N + 1)); 

  % The might exist objects, that don't appear in any image,
  % in such a case, [df] would be 0.
  if df > 0
    l2 = log2((N + 1) / df);
  else
    l2 = 0;
  end
  score = tf_star * l2;
end
