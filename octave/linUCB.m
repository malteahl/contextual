function bestarms = linUCB(X, gt, alpha = 1.65, loopnr = 3)
%% The linUCB() algorithm as proposed in 
%% "A Contextual-Bandit Approach to Personalized 
%% News Article Recommendation", Lihong Li, Wei Chu, 
%% Yahoo! Labs
%% Copyright Malte Ahl

  % Dimension of the context vector.
  d     = size(X{1})(1);

  % Number of tests.
  T     = size(X{1})(2);
  
  % Number of arms/models of our multi armed bandit.
  anum  = numel(gt(1, :));

  %%%% ASSERTS %%%%%
  assert(size(hidden)(1) == 1);
  assert(size(gt) == [T numel(X)]);
  assert(numel(X) == anum);
  for arm = 1:anum
    assert(size(X{arm}) == [d T]);
  end
  %%%%%%%%%%%%%%%%%%

  % Best arms/models.
  bestarms = zeros(1, T);
  
  % Create new arms a := {id(d), b}.
  arms = {};
  for a = 1:anum
    arms{end+1} = newarm(d);
  end
  
  % Run the contextual LinUCB strategy.
  for loop = 1:(loopnr+1)
    for t = 1:T
      p = zeros(1, anum);
      for a = 1:anum
        A = arms{a}{1};
        b = arms{a}{2};

        % Compute UCB for the current arm.
        iA      = inverse(A);
        theta   = iA * b;
        x       = X{a}(:, t);
        p(a)    = dot(theta, x) + alpha * sqrt(dot(x' * iA, x));
      end

      % Apply LinUCB on all arms.
      val            = max(p);
      % Look for entries equal val (ties broken arbitrarily),
      % because max retrieves only the id of first  best value.
      bestids        = find(sqrt((val - p).^2) < 0.0001);
      rid            = unidrnd(numel(bestids));
      best           = bestids(rid);
      bestarms(1, t) = best;

      % Observe reward for the current best arm.
      r = gt(t, best);

      % Update the current best arm.
      A     = arms{best}{1};
      b     = arms{best}{2};
      x     = X{best}(:, t);
      A_new = A + (x * x');
      b_new = b + (r * x);
      arms{best}{1} = A_new;
      arms{best}{2} = b_new;
    end
  end
end
