function Q = bm25(run='', A, b= 0.75, k = 1.75)
% Computes the bm25 values, given a term frequency matrix.
% Copyright Malte Ahl.
  if cellfun('isempty',{run})
    printf('\nUSAGE:\n');
    printf('score = bm25(run, A, b = 0.75, k = 1.75)\n\n');
    printf('run = if empty string, show this hint.\n');
    printf('A = n x m matrix with term frequency values.\n');
    printf('b = parameter b from bm25. Good value b = 0.75 .\n');
    printf('k = parameter k from bm25. Good value k = 1.75 .\n');
    printf('\n');
    return;
  end
  Q = zeros(size(A));

  N = size(A)(2);
  AIS = sum(sum(A)) / N;

  for obj = [1:size(A)(1)]
    for image = [1:size(A)(2)]
      df = numel(nonzeros(A(obj,:)));
      tf = A(obj,image);
      noi = sum(A(:,image));
      s = tfidf('run', tf, b, k, noi, AIS, N, df);
      Q(obj, image) = s;
    end
  end
end
