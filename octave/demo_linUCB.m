function demo_linUCB()
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % linUCB() %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% The linUCB() algorithm as proposed in 
  %% "A Contextual-Bandit Approach to Personalized 
  %% News Article Recommendation", Lihong Li, Wei Chu, 
  %% Yahoo! Labs


  % TEST-WORLD for linUCB.
  % Our test world has 10 user queries / context vectors.
  % Every context vector describes the a state of our world
  % with 5 values.Every arm/model has its own set of contextes 
  % (this is the "disjoint linear model" requirement of linUCB).
  numContext = 10;
  dimContext = 5;
  X1 = unifrnd(0, 1, dimContext, numContext);
  X2 = unifrnd(0, 1, dimContext, numContext);
  X3 = unifrnd(0, 1, dimContext, numContext);
  X4 = unifrnd(0, 1, dimContext, numContext);
  X  = {X1, X2, X3, X4};

  % Define a ground truth with real valued rewards, defining
  % the best performing model of our 4 models
  % for every of our 10 contexes.
  gt = [eye(4, 4); eye(4,4); eye(2,4)];
  test     = [1:4 1:4 1 2];

  maxLoop = 60;
  loops = [1 2 3 4 5 10:10:maxLoop];
  maxTests = numel(loops);
  success = zeros(1, maxTests);

  for i = 1:maxTests
    bestarms = linUCB(X, gt, alpha = 1.65, loops(i));

    % We assume, that linUCB finds the best model for every
    % context after iterating 100 times over the same 
    % context set. (This assumption is optimistic !)
    numFails = numel(nonzeros(test - bestarms));
    success(i) = numContext - numFails;
  end

  % Results.
  % Some statistics.
  printf('\n\nINPUT (random generated):\n');
  dimstr = ['dxc matrix, d=' num2str(dimContext) ', c=' num2str(numContext)];
  printf(['Context of arm/model 1 (' dimstr '):\n']);
  printf('%1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f\n', X1);
  printf(['\nContext of arm/model 2 (' dimstr '):\n']);
  printf('%1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f\n', X2);
  printf(['\nContext of arm/model 3 (' dimstr '):\n']);
  printf('%1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f\n', X3);
  printf(['\nContext of arm/model 4 (' dimstr '):\n']);
  printf('%1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f %1.2f\n', X4);
  printf('\n\nSTATISTICS:\n');
  printf(['Number of arms/models: ' num2str(numel(X)) '\n']);
  printf(['Number of contexts [c]: ' num2str(numContext) '\n']);
  printf(['Dimension of [d] of conetxt vector: ' num2str(dimContext) '\n']);

  % Pint.
  bar(success);
  set (gca, 'xticklabel', loops);
  legend('linUCB');
  xlabel('# iterations'); 
  ylabel('successful model selections');

end



