function arm = newarm(d)
  % Return a new arm for linUCB().
  % Copyright Malte Ahl
  A = eye(d);
  b = zeros(d, 1);

  arm = {A, b};
end
