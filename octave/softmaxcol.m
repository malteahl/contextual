function result = softmaxcol(vec)
% Calculates the softmax value column-wise.
% Copyright Malte Ahl
  for i = [1:size(vec)(2)]
    for j = [1:size(vec)(1)]
      v = vec(j,i);
      if v > 0
        result(j,i) = exp(vec(j,i));
      else 
        result(j,i) = 0;
      end
    end
    sum_i = sum(result(:,i));
    if sum_i > 0
      result(:,i) = result(:, i) ./ sum_i;
    else 
      result(:,i) = zeros(size(vec)(1), 1);
    end
  end
end
