function runtestcases()
  % This function test every other octave function in this directory
  % and should be called after every change.
  % Copyright Malte Ahl

  % Epsilon.
  eps = 0.0001;

  % Some matrices for testing.

  B = [1 3 1; 30 30 30; 0 5 5];
  C = [1 1 1; 2 2 2; 3 3 3];

  D(1, 1) = 1.057692308 * 0.415037499;
  D(1, 2) = 1.696658098 * 0.415037499;
  D(1, 3) = 0.986547085 * 0.415037499;
  D(2, 1) = 2.610759494 * 0.415037499;
  D(2, 2) = 2.589250687 * 0.415037499;
  D(2, 3) = 2.595359811 * 0.415037499;
  D(3, 1:3) = [0 2.003642987 2.025782689];

  E = [0.25 0.25 0.25; 0.25 0.25 0.25; 0 0.5 0.5];

  F = [zeros(3,4); ones(1,4)];

  G = [10 10 10; 9 0 0; 0 0 9];

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%% ASSERTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % bm25() %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % See H.Bast, Uni Freiburg, Information Retrieval.
  %BM25 score = tf* · log2 ((N+1) / df), where
  %tf* = tf · (k + 1) / (k · (1 – b + b · DL / AVDL) + tf)
  %for b = 0.75 and k = 1.75
  bs = bm25('run', B);
  b = sum(sum(sqrt((D.-bs).^2) < eps )) == numel(B);
  assert(b, 'TESTCASE01: bm25() failed.');
  printf('bm25() ok\n');

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % tfidf() %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  AVDL = 35;     % Average Document Length.
  N    = 3;      % Number of images.
  b    = 0.75;   % param b of bm25.
  k    = 1.75;   % param k of bm25.
  DL   = 38;     % Number of objects for on image.
  df   = 2;      % Number of images containing the object.
  tf   = 5;      % Object frequency (eq to bm25 termfreq.)
  r    = tfidf('run', tf, b, k, DL, AVDL, N, df);
  b    = sqrt((D(3, 2) - r)^2) <= eps; 
  assert(b, 'TESTCASE: tfidf() failed.');
  printf('tfidf() ok\n');

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % softmax() %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  vec1   = [5 4 3 2 1];
  vec2   = sort(vec1);
  mat    = [vec1; vec2];
  test1  = [0.636409 0.234122 0.086129 0.031685 0.011656];
  test2  = sort(test1);
  test   = [test1; test2];
  res    = softmax(mat);
  serr1   = sqrt((test(1,:) .- res(1,:)).^2); 
  serr2   = sqrt((test(2,:) .- res(2,:)).^2); 
  err01  = 'TESTCASE01: softmax() failed.';
  assert(sum(serr1 < eps) == numel(mat(1,:)), err01);
  assert(sum(serr2 < eps) == numel(mat(2,:)), err01);
  printf('softmax() ok\n');

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % softmaxcol() %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  vec1   = [5; 4; 3; 2; 1];
  vec2   = sort(vec1);
  mat    = [vec1 vec2];
  test1  = [0.636409; 0.234122; 0.086129; 0.031685; 0.011656];
  test2  = sort(test1);
  test   = [test1 test2];
  res    = softmaxcol(mat);
  res    = res;
  serr1   = sqrt((test(:,1) .- res(:,1)).^2); 
  serr2   = sqrt((test(:,2) .- res(:,2)).^2); 
  err01  = 'TESTCASE01: softmaxcol() failed.';
  assert(sum(serr1 < eps) == numel(mat(:,1)), err01);
  assert(sum(serr2 < eps) == numel(mat(:,2)), err01);
  printf('softmaxcol() ok\n');

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % norm_mat() %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  a       = [1 2 3; 4 1 2; 1 8 3];
  res     = sum(norm_mat(a));
  diff    = sqrt((res - ones(1, 3)).^2) < 0.00001;
  assert(sum(diff) == 3, 'TESTCASE01: norm_mat() failed.');
  printf('norm_mat() ok\n');

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % linUCB() %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% The linUCB() algorithm as proposed in 
  %% "A Contextual-Bandit Approach to Personalized 
  %% News Article Recommendation", Lihong Li, Wei Chu, 
  %% Yahoo! Labs


  % TEST-WORLD for linUCB
  % Our test world has 10 user queries / context vectors.
  % Every context vector describes the a state of our world
  % with 5 values.Every arm/model has its own set of contextes 
  % (this is the "disjoint linear model" requirement of linUCB).
  X1 = unifrnd(0, 1, 5, 10);
  X2 = unifrnd(0, 1, 5, 10);
  X3 = unifrnd(0, 1, 5, 10);
  X4 = unifrnd(0, 1, 5, 10);
  X  = {X1, X2, X3, X4};

  % Define a ground truth with real valued rewards, defining
  % the best performing model of our 4 models
  % for every of our 10 contexes.
  gt = [eye(4, 4); eye(4,4); eye(2,4)];

  bestarms = linUCB(X, gt, 1.65, 100);

  % I assume, that linUCB finds the best model for every
  % context after iterating 100 times over the same 
  % context set. (This assumption is optimistic !)
  test     = [1:4 1:4 1 2];
  numOfFails = numel(nonzeros(test - bestarms));
  b        = numOfFails < 4;
  assert(b, 'linUCB() failed.');

  printf('linUCB() ok\n');



  printf('\n\nDONE (ALL TESTS OK)\n');

end
